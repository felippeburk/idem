import pytest_idem.runner as runner
import yaml


def test_config(idem_cli):

    # Create a config template
    proc_template = idem_cli(
        "state",
        "--config-template",
        "--cache-dir=cache",
        check=True,
        encoding="ascii",
    )

    # Verify that output was created
    assert proc_template.stdout
    # Verify that the output is valid yaml
    assert yaml.safe_load(proc_template.stdout)

    with runner.named_tempfile(suffix=".cfg", mode="w+") as fh:
        # Write the config template to a config file
        fh.write_text(proc_template.stdout)

        # Run an idem process with the --config flag after the subcommand
        ret = idem_cli(
            "exec",
            "test.ping",
            "--output=json",
            f"--config={fh}",
            encoding="ascii",
            check=True,
        ).json
        assert ret == {
            "comment": None,
            "ref": "exec.test.ping",
            "result": True,
            "ret": True,
        }

        # Run an idem process with the --config flag before the subcommand
        ret = idem_cli(
            "exec",
            "test.ping",
            "--output=json",
            pre_subcommand_args=[f"--config={fh}"],
            encoding="ascii",
            check=True,
        ).json
        assert ret == {
            "comment": None,
            "ref": "exec.test.ping",
            "result": True,
            "ret": True,
        }
