import os
import pathlib

import dict_tools.data
import pytest
import pytest_idem.runner as runner


@pytest.fixture(name="ctx")
def local_ctx(tmpdir):
    return dict_tools.data.NamespaceDict(
        acct=dict(
            run_name="test",
            cache_dir=pathlib.Path(tmpdir),
            serial_plugin="msgpack",
        )
    )


async def test_context_existing(hub, tmpdir):
    """
    With a nested context, the pid will already exist when the second context runs
    """
    async with hub.idem.managed.context("test", tmpdir / "exists"):
        with pytest.raises(RuntimeError) as e:
            async with hub.idem.managed.context("test", tmpdir / "exists"):
                ...
            assert (
                "Fail to enter enforced state management: idem run 'test' is already active in process"
                in str(e)
            )


async def test_different_context(hub, tmpdir):
    """
    With different run_names,
    """
    async with hub.idem.managed.context("test", tmpdir / "1"):
        async with hub.idem.managed.context("different", tmpdir / "2"):
            ...


async def test_context(hub, ctx):
    """
    Verify that the pid_file exists in the context with an integer
    """
    path: pathlib.Path = await hub.esm.local.enter(ctx)
    try:
        with path.open("r") as fh:
            assert int(fh.read()) == os.getpid()
    finally:
        path.unlink()


async def test_verify_exists(hub):
    pid_file = pathlib.Path("non-existent")
    assert await hub.esm.local.verify(pid_file) is None
    assert not pid_file.exists()


async def test_verify_contents_valid(hub):
    with runner.named_tempfile(suffix=".pid") as fh:
        fh.write_text(str(os.getpid()))

        pid_file = pathlib.Path(fh)

        assert await hub.esm.local.verify(pid_file) == os.getpid()
        assert pid_file.exists()

    assert not pid_file.exists()


async def test_verify_contents_invalid(hub):
    with runner.named_tempfile(suffix=".pid") as fh:
        fh.write_text("crash")

        pid_file = pathlib.Path(fh)

        assert await hub.esm.local.verify(pid_file) is None
        assert pid_file.exists()

    assert not pid_file.exists()


async def test_set_state(hub, ctx):
    await hub.esm.local.set_state(ctx, {"key": "value"})
    ret = await hub.esm.local.get_state(ctx)
    assert ret == {"key": "value"}
