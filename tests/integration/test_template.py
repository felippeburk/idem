from pytest_idem.runner import run_sls


def test_template_render(hub):
    ret = run_sls(["template"])
    assert len(ret) == 6, "Expecting 6 result"

    # assert for template_render_with_template_file_name
    assert (
        ret[
            "template_|-template_render_with_template_file_name_|-template_render_test-1_|-render"
        ]["result"]
        is True
    )

    assert (
        "Template rendering is success"
        == ret[
            "template_|-template_render_with_template_file_name_|-template_render_test-1_|-render"
        ]["comment"][0]
    )

    assert (
        ret[
            "template_|-template_render_with_template_file_name_|-template_render_test-1_|-render"
        ]["new_state"]["rendered"]
        == "Hello test-idem !!"
    )

    # assert for template_render_with_template
    assert (
        ret[
            "template_|-template_render_with_template_|-template_render_test-2_|-render"
        ]["result"]
        is True
    )

    assert (
        "Template rendering is success"
        == ret[
            "template_|-template_render_with_template_|-template_render_test-2_|-render"
        ]["comment"][0]
    )

    assert (
        ret[
            "template_|-template_render_with_template_|-template_render_test-2_|-render"
        ]["new_state"]["rendered"]
        == "Hello test-idem !!"
    )

    # assert for template_render_with_wrong_template_file_name
    assert (
        ret[
            "template_|-template_render_with_wrong_template_file_name_|-template_render_test-3_|-render"
        ]["result"]
        is False
    )

    assert (
        "Template file not found: ./tests/sls/templates/abc.tpl"
        == ret[
            "template_|-template_render_with_wrong_template_file_name_|-template_render_test-3_|-render"
        ]["comment"][0]
    )

    assert not (
        ret[
            "template_|-template_render_with_wrong_template_file_name_|-template_render_test-3_|-render"
        ]["new_state"]
    )

    # assert for template_render_when_both_template_file_name_and_template_is_none
    assert (
        ret[
            "template_|-template_render_when_both_template_file_name_and_template_is_none_|-template_render_test-4_|-render"
        ]["result"]
        is False
    )

    assert (
        "Either template_file_name or template should be provided"
        == ret[
            "template_|-template_render_when_both_template_file_name_and_template_is_none_|-template_render_test-4_|-render"
        ]["comment"][0]
    )

    assert not (
        ret[
            "template_|-template_render_when_both_template_file_name_and_template_is_none_|-template_render_test-4_|-render"
        ]["new_state"]
    )

    # assert for template_render_with_template_file_name_but_no_variables
    assert (
        ret[
            "template_|-template_render_with_template_file_name_but_no_variables_|-template_render_test-5_|-render"
        ]["result"]
        is True
    )

    assert (
        "Template rendering is success"
        == ret[
            "template_|-template_render_with_template_file_name_but_no_variables_|-template_render_test-5_|-render"
        ]["comment"][0]
    )

    assert (
        ret[
            "template_|-template_render_with_template_file_name_but_no_variables_|-template_render_test-5_|-render"
        ]["new_state"]["rendered"]
        == "Hello World !!"
    )

    # assert for template_render_with_template_but_no_variables
    assert (
        ret[
            "template_|-template_render_with_template_but_no_variables_|-template_render_test-6_|-render"
        ]["result"]
        is True
    )

    assert (
        "Template rendering is success"
        == ret[
            "template_|-template_render_with_template_but_no_variables_|-template_render_test-6_|-render"
        ]["comment"][0]
    )
    assert (
        ret[
            "template_|-template_render_with_template_but_no_variables_|-template_render_test-6_|-render"
        ]["new_state"]["rendered"]
        == "Hello World !!"
    )
