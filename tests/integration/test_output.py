def test_output_func_counts(code_dir, idem_cli):
    sls = code_dir.joinpath("tests", "sls", "output.sls")
    ret = idem_cli("state", sls, "--output=state", check=True).stdout

    assert "nop: 3 successful" in ret
    assert "fail_without_changes: 2 failed" in ret
    assert "anop: 1 successful" in ret
    assert "present: 2 created successfully" in ret
    assert "present: 1 updated successfully" in ret
    assert "present: 1 no-op" in ret
    assert "absent: 2 no-op" in ret
