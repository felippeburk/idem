__contracts__ = ["exec.contracts.returns"]


def fail(hub):
    raise ValueError("Fail")


def malformed(hub):
    return False


def malformed_dict(hub):
    return {}


def wrong_status_type(hub):
    return dict(result="true", comment="", ret=object())


def ping(hub):
    return dict(result=True, comment="", ret=...)
